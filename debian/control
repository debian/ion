Source: ion
Priority: extra
Maintainer: Leo Iannacone <l3on@ubuntu.com>
Build-Depends:
 debhelper (>= 9),
 dh-autoreconf,
 ghostscript,
 groff,
 libexpat1-dev,
 procps,
 psutils
Standards-Version: 3.9.6
Section: net
Homepage: https://ion.ocp.ohiou.edu/
Vcs-Git: https://salsa.debian.org/debian/ion.git
Vcs-Browser: https://salsa.debian.org/debian/ion

Package: ion
Architecture: any
Depends:
 libion0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Suggests:
 ion-doc
Description: NASA implementation of Delay-Tolerant Networking (DTN)
 Interplanetary Overlay Network (ION) software distribution
 is an implementation of Delay-Tolerant Networking (DTN)
 architecture as described in Internet RFC 4838.
 .
 This is a suite of communication protocol implementations designed
 to support mission operation communications across an end-to-end
 interplanetary network, which might include on-board (flight) subnets,
 in-situ planetary or lunar networks, proximity links,
 deep space links, and terrestrial internets.
 .
 Included in the ION software distribution are the following packages:
  * ici (interplanetary communication infrastructure) a set of libraries
    that provide flight-software-compatible support for functions on
    which the other packages rely
  * bp (bundle protocol), an implementation of the Delay-Tolerant
    Networking (DTN) architecture's Bundle Protocol.
  * dgr (datagram retransmission), a UDP reliability system that implements
    congestion control and is designed for relatively high performance.
  * ltp (licklider transmission protocol), a DTN convergence layer for reliable
    transmission over links characterized by long or highly variable delay.
  * ams - an implementation of the CCSDS Asynchronous Message Service.
  * cfdp - a class-1 (Unacknowledged) implementation of the CCSDS File
    Delivery Protocol.
  .
  This package contains the binary files.

Package: libion0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${shlibs:Depends}
Description: NASA implementation of Delay-Tolerant Networking (DTN) - main libraries
 Interplanetary Overlay Network (ION) software distribution
 is an implementation of Delay-Tolerant Networking (DTN)
 architecture as described in Internet RFC 4838.
 .
 This is a suite of communication protocol implementations designed
 to support mission operation communications across an end-to-end
 interplanetary network, which might include on-board (flight) subnets,
 in-situ planetary or lunar networks, proximity links,
 deep space links, and terrestrial internets.
 .
 Included in the ION software distribution are the following packages:
  * ici (interplanetary communication infrastructure) a set of libraries
 that provide flight-software-compatible support for functions on
 which the other packages rely
  * bp (bundle protocol), an implementation of the Delay-Tolerant
 Networking (DTN) architecture's Bundle Protocol.
  * dgr (datagram retransmission), a UDP reliability system that implements
 congestion control and is designed for relatively high performance.
  * ltp (licklider transmission protocol), a DTN convergence layer for reliable
 transmission over links characterized by long or highly variable delay.
  * ams - an implementation of the CCSDS Asynchronous Message Service.
  * cfdp - a class-1 (Unacknowledged) implementation of the CCSDS File
 Delivery Protocol.
  .
  This package contains the library files.

Package: libion-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libion0 (= ${binary:Version}),
 ${misc:Depends}
Suggests: ion-doc
Description: NASA implementation of Delay-Tolerant Networking (DTN) - development files
 Interplanetary Overlay Network (ION) software distribution
 is an implementation of Delay-Tolerant Networking (DTN)
 architecture as described in Internet RFC 4838.
 .
 This is a suite of communication protocol implementations designed
 to support mission operation communications across an end-to-end
 interplanetary network, which might include on-board (flight) subnets,
 in-situ planetary or lunar networks, proximity links,
 deep space links, and terrestrial internets.
 .
 Included in the ION software distribution are the following packages:
  * ici (interplanetary communication infrastructure) a set of libraries
 that provide flight-software-compatible support for functions on
 which the other packages rely
  * bp (bundle protocol), an implementation of the Delay-Tolerant
 Networking (DTN) architecture's Bundle Protocol.
  * dgr (datagram retransmission), a UDP reliability system that implements
 congestion control and is designed for relatively high performance.
  * ltp (licklider transmission protocol), a DTN convergence layer for reliable
 transmission over links characterized by long or highly variable delay.
  * ams - an implementation of the CCSDS Asynchronous Message Service.
  * cfdp - a class-1 (Unacknowledged) implementation of the CCSDS File
 Delivery Protocol.
  .
  This package contains the development files.

Package: ion-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends}
Recommends:
 ion
Description: Interplanetary Overlay Network - examples and documentation
 Interplanetary Overlay Network (ION) software distribution
 is an implementation of Delay-Tolerant Networking (DTN)
 architecture as described in Internet RFC 4838.
 .
 This is a suite of communication protocol implementations designed
 to support mission operation communications across an end-to-end
 interplanetary network, which might include on-board (flight) subnets,
 in-situ planetary or lunar networks, proximity links,
 deep space links, and terrestrial internets.
 .
 Included in the ION software distribution are the following packages:
  * ici (interplanetary communication infrastructure) a set of libraries
    that provide flight-software-compatible support for functions on
    which the other packages rely
  * bp (bundle protocol), an implementation of the Delay-Tolerant
    Networking (DTN) architecture's Bundle Protocol.
  * dgr (datagram retransmission), a UDP reliability system that implements
    congestion control and is designed for relatively high performance.
  * ltp (licklider transmission protocol), a DTN convergence layer for reliable
    transmission over links characterized by long or highly variable delay.
  * ams - an implementation of the CCSDS Asynchronous Message Service.
  * cfdp - a class-1 (Unacknowledged) implementation of the CCSDS File
    Delivery Protocol.
  .
  This package contains the documentation and some configuration example.
